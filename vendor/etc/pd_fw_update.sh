#!/vendor/bin/sh
type=`getprop vendor.asus.dongletype`
if [ "$type" != "8" ]; then
	echo "[AURA_INBOX] FANDG6 didn't exist. Type is $type" > /dev/kmsg
	setprop vendor.fandg.pd_fwupdate 0
    exit
fi

update_status=`getprop vendor.asus.accy.fw_status`
if [ "$update_status" == "003000" ]; then
	echo "[AURA_INBOX] PD is updating " > /dev/kmsg
    exit
fi

#fw_ver=`cat /sys/class/leds/aura_inbox/pd_fw_ver`
pd_asusfw_ver=`getprop vendor.asusfw.fandg6.pd_fwver`
setprop vendor.asus.accy.fw_status 003000
echo "[AURA_INBOX] PD update firmware start, current ver:$fw_ver, target ver:$pd_asusfw_ver" > /dev/kmsg
echo 1 > /sys/class/leds/aura_inbox/pd_fw_update
echo "[AURA_INBOX] PD update firmware end" > /dev/kmsg
sleep 3

#check fandongle reconnect
type=`getprop vendor.asus.dongletype`
while [ "$type" != "8" ]
do
	type=`getprop vendor.asus.dongletype`
	sleep 1;
	echo "[AURA_INBOX] Wait for fandg reconnect...." > /dev/kmsg
done


echo "[AURA_INBOX] PD update firmware verify......" > /dev/kmsg
verify_result=`cat /sys/class/leds/aura_inbox/pd_fw_check`
if [ "$verify_result" = "1" ]; then
	echo "[AURA_INBOX] PD update firmware verify pass" > /dev/kmsg
	setprop vendor.fandg.pd_fwupdate 0
else
	echo "[AURA_INBOX] PD update firmware verify fail" > /dev/kmsg
	setprop vendor.fandg.pd_fwupdate 2
fi

#check fandongle reconnect
#type=`getprop vendor.asus.dongletype`
#while [ "$type" != "8" ]
#do
#	type=`getprop vendor.asus.dongletype`
#	sleep 1;
#done

#fw_ver=`cat /sys/class/leds/aura_inbox/pd_fw_ver`

#if [ "$fw_ver" == "$pd_asusfw_ver" ]; then
#	setprop vendor.fandg.pd_fwupdate 0
#	echo "[AURA_INBOX] PD ver: $fw_ver" > /dev/kmsg
#else
#	setprop vendor.fandg.pd_fwupdate 2
#	echo "[AURA_INBOX] PD firmware update failed" > /dev/kmsg
#fi
