#!/vendor/bin/sh

dongle_type=`getprop vendor.asus.dongletype`
mic_type=`getprop vendor.asus.fan.mic`
user_config=`getprop persist.vendor.asus.micfansettings`

if [ "$dongle_type" == "1" -o "$dongle_type" == "8" ]; then
	if [ "$user_config" == "1" ] && [ "$mic_type" == "1" ]; then
		echo "[ROG6_INBOX] turn off Fan cause by mic enable" > /dev/kmsg
		echo 0 > /sys/class/leds/aura_inbox/fan_RPM
	else
		fan_rpm=`getprop persist.vendor.asus.userfanrpm`
		echo "[ROG6_INBOX] set fan RPM = $fan_rpm" > /dev/kmsg
		echo $fan_rpm > /sys/class/leds/aura_inbox/fan_RPM
	fi
fi

