#!/vendor/bin/sh

type=`getprop vendor.asus.dongletype`
sleep 3
echo "[ROG_ACCY] Get Dongle FW Ver, type $type" > /dev/kmsg

if [ "$type" == "0" ]; then
#	phone_aura=`cat /sys/class/leds/aura_sync/fw_ver`
#	setprop sys.phone.aura_fwver $phone_aura

	setprop vendor.inbox.aura_fwver 0
	
	setprop vendor.asus.accy.fw_status 000000
	setprop vendor.asus.accy.fw_status2 000000
elif [ "$type" == "1" ]; then
	inbox_aura=`cat /sys/class/leds/aura_inbox/fw_ver`
	setprop vendor.inbox.aura_fwver $inbox_aura

	# check FW need update or not
	aura_fw=`getprop vendor.asusfw.inbox.aura_fwver`
	if [ "$inbox_aura" == "$aura_fw" ]; then
		setprop vendor.asus.accy.fw_status 000000
	elif [ "$inbox_aura" == "i2c_error" ]; then
		echo "[ROG_ACCY] InBox AURA_SYNC FW Ver Error" > /dev/kmsg
		setprop vendor.asus.accy.fw_status 000000
	else
		setprop vendor.asus.accy.fw_status 100000
	fi
elif [ "$type" == "8" ]; then
	mcu_download_mode=`cat /sys/class/leds/aura_inbox/NUC1261_ap2ld`
	echo "[ROG_ACCY] get mcu_download_mode = $mcu_download_mode" > /dev/kmsg

	if [ "$mcu_download_mode" == "1" ]; then
		echo "[ROG_ACCY] MCU damage, force to update firmware" > /dev/kmsg
		setprop vendor.asus.accy.fw_status 010000
	else
		sleep 1
		inbox_aura=`cat /sys/class/leds/aura_inbox/fw_ver`
		setprop vendor.inbox.aura_fwver $inbox_aura
		sleep 0.5
		inbox_mcu1261=`cat /sys/class/leds/aura_inbox/NUC1261_fw_ver`
		setprop vendor.inbox.inbox_fwver $inbox_mcu1261
		sleep 0.5
		inbox_pd=`cat /sys/class/leds/aura_inbox/pd_fw_ver`
		setprop vendor.inbox.pd_fwver $inbox_pd
		echo "[ROG_ACCY] Fandongle 6 ver: $inbox_mcu1261 $inbox_aura $inbox_pd" > /dev/kmsg

		# check FW need update or not
		aura_fw=`getprop vendor.asusfw.fandg6.aura_fwver`
		inbox_fw=`getprop vendor.asusfw.fandg6.inbox_fwver`
		pd_fw=`vendor.asusfw.fandg6.pd_fwver`

		#echo "[ROG_ACCY] asusfw = $aura_fw-$inbox_fw-$pd_fw" > /dev/kmsg
		#echo "[ROG_ACCY] inboxfw = $inbox_aura-$inbox_mcu1261-$inbox_pd" > /dev/kmsg
		if [ "$inbox_aura" == "$aura_fw" ] && [ "$inbox_mcu1261" == "$inbox_fw" ] && [ "$inbox_pd" == "$pd_fw" ]; then
			echo "[ROG_ACCY] InBox no need to update" > /dev/kmsg
			setprop vendor.asus.accy.fw_status 000000
			exit
		elif [ "$inbox_aura" == "i2c_error" ] || [ "$inbox_mcu1261" == "i2c_error" ] || [ "$inbox_pd" == "i2c_error" ]; then
			echo "[ROG_ACCY] InBox AURA_SYNC FW Ver Error 1" > /dev/kmsg
			setprop vendor.asus.accy.fw_status 000000
			exit
		elif [ ! -n "$inbox_aura" ] || [ ! -n "$inbox_mcu1261" ] || [ ! -n "$inbox_pd" ] || [ ! -n "$aura_fw" ] || [ ! -n "$inbox_fw" ]; then
			echo "[ROG_ACCY] InBox AURA_SYNC FW Ver Error 2" > /dev/kmsg
			setprop vendor.asus.accy.fw_status 000000
			exit
		elif [ ! -n "$aura_fw" ] || [ ! -n "$inbox_fw" ]; then
			echo "[ROG_ACCY] InBox Get build in firmware error" > /dev/kmsg
			setprop vendor.asus.accy.fw_status 000000
			exit
		elif [ "$inbox_aura" == "0x0000" ] || [ "$inbox_mcu1261" == "0x0000" ] || [ "$inbox_pd" == "0x0000" ]; then
			echo "[ROG_ACCY] InBox AURA_SYNC FW Ver Error 3" > /dev/kmsg
			setprop vendor.asus.accy.fw_status 000000
			exit
		fi
	
		if [ "$inbox_aura" != "$aura_fw" ]; then
			echo "[ROG_ACCY] update inbox Aura from [$inbox_aura] to [$aura_fw]" > /dev/kmsg
			setprop vendor.asus.accy.fw_status 100000
			#echo "[ROG_ACCY] AURA update not support" > /dev/kmsg
		fi

		if [ "$inbox_mcu1261" != "$inbox_fw" ]; then
			echo "[ROG_ACCY] update inbox MCU from [$inbox_mcu1261] to [$inbox_fw]" > /dev/kmsg
			setprop vendor.asus.accy.fw_status 010000
			#echo "[ROG_ACCY] MCU update not support" > /dev/kmsg
		fi

		#if [ "$inbox_pd" != "$pd_fw" ]; then
		#	echo "[ROG_ACCY] update inbox PD" > /dev/kmsg
		#	setprop vendor.asus.accy.fw_status 001000
		#fi
		echo "[ROG_ACCY] PD update not support" > /dev/kmsg
	fi
fi

echo "[ROG_ACCY] Get Dongle FW Ver done." > /dev/kmsg
