#!/system/bin/sh

LCD_UNIQUE_ID=$(cat /proc/lcd_unique_id)
MCF_FILE_100=/data/media/0/${LCD_UNIQUE_ID}_100.mcf
MCF_FILE_500=/data/media/0/${LCD_UNIQUE_ID}_500.mcf
TARGET=/logbuf/

cp $MCF_FILE_100 $TARGET
sleep 1
cp $MCF_FILE_500 $TARGET
sleep 1
rm $MCF_FILE_100
rm $MCF_FILE_500
chmod 666 $TARGET/${LCD_UNIQUE_ID}_100.mcf
chmod 666 $TARGET/${LCD_UNIQUE_ID}_500.mcf
setprop debug.asus.mcf.repair 2

